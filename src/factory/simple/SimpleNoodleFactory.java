package factory.simple;

public class SimpleNoodleFactory {
    private static final int TYPE_LZ = 0;
    private static final int TYPE_PAO  =1;

    public static INoodles createNoodle(int type) {
        switch (type) {
            case TYPE_LZ:
                return new LzNoodles();
            case TYPE_PAO:
                return new FastNoodle();
                default:
                    return null;
        }
    }
}
