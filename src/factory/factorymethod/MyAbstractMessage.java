package factory.factorymethod;

import java.util.Map;

public abstract class MyAbstractMessage implements IMyMessage {
    private Map<String, Object> messageParam;

    @Override
    public Map<String, Object> getMessageParam() {
        return messageParam;
    }

    @Override
    public void setMessageParam(Map<String, Object> messageParam) {
        this.messageParam = messageParam;
    }
}
