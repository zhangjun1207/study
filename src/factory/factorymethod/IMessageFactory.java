package factory.factorymethod;

public interface IMessageFactory {
    public IMyMessage createMessage(String messageType);
}
